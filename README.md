# daemonize

A tool to run a command as a daemon

## Home Page

http://software.clapper.org/daemonize/

## Github

https://github.com/bmc/daemonize

## Download

You can download **daemonize** archlinux package from here:

* [daemonize-x86_64.pkg.tar.zst](https://gitlab.com/archlinux_build/daemonize/-/jobs/artifacts/main/browse?job=run-build)

## Install

eg. v1.7.8

```bash
sudo pacman -U daemonize-1.7.8-1-x86_64.pkg.tar.zst
```
